import requests

# Replace YOUR_ACCESS_TOKEN with your LINE channel access token
access_token = "zWkaac7s2oLNx2XgLTfJyv8DtZY/JUzrcDbbiuI57glAXwdrFwLrFY7mmtOOfQETJyoAO3C3rxpKRF9TGGSqMrKAkR/UgoczGCqhHx75OfauN1FBm2J80CDHxjgTyV6wG5bLKKOKak/0YndNiyGbhgdB04t89/1O/w1cDnyilFU="

# Replace user_ids with a list of user IDs for which you want to retrieve profiles
user_ids = ["U0064e29d230d66cbc5772dd9196a5194"]

for user_id in user_ids:
    url = f"https://api.line.me/v2/bot/profile/{user_id}"
    headers = {"Authorization": f"Bearer {access_token}"}

    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        profile_data = response.json()
        display_name = profile_data.get("displayName")
        picture_url = profile_data.get("pictureUrl")
        print(f"User ID: {user_id}, Display Name: {display_name}, Picture URL: {picture_url}")
    else:
        print(f"Error for User ID {user_id}: {response.status_code} - {response.text}")
